I chose the MVC as major architecture for the application.

In this, the ViewControllers are responsible for synchronising between views (UILabel, UITextField, etc.) and data.  This architechture may be easy to implement but it has huge drawback (really huge) that is massive ViewController.

The currency exchange rates is fetched using api from European Central Bank. This api is free for use.
See details at: https://exchangeratesapi.io.

I did not use storyboard for designing UI. Instead, I hard-code the UI elements using lazy initialisation supported by Swift. With this, I can avoid painful merge conflict in storyboad, also easier code-review.


