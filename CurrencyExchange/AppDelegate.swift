//
//  AppDelegate.swift
//  CurrencyExchange
//
//  Created by Cuong Nguyen on 10/17/19.
//  Copyright © 2019 Cuong Nguyen. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let navVC = UINavigationController(rootViewController: CurrencyConversionViewController())
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        window?.rootViewController = navVC
        
        return true
    }

}

