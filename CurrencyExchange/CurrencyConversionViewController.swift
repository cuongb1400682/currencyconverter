//
//  ViewController.swift
//  CurrencyExchange
//
//  Created by Cuong Nguyen on 10/17/19.
//  Copyright © 2019 Cuong Nguyen. All rights reserved.
//

import UIKit

class CurrencyConversionViewController: UIViewController {
    private lazy var fromLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14, weight: .light)
        label.textColor = .white
        label.text = "FROM"
        return label
    }()
    
    private lazy var fromCurrency: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont.systemFont(ofSize: 24, weight: .bold)
        button.setTitleColor(.white, for: .normal)
        button.setTitle("USD", for: .normal)
        button.addTarget(self, action: #selector(showPicker(_:)), for: .touchUpInside)
        return button
    }()
    
    private lazy var fromCurrencyInput: UITextField = {
        let textField = UITextField()
        textField.text = "0"
        textField.textColor = .white
        textField.font = UIFont.systemFont(ofSize: 40, weight: .light)
        textField.textAlignment = .right
        textField.adjustsFontSizeToFitWidth = true
        textField.minimumFontSize = 7.0
        textField.delegate = self
        return textField
    }()
    
    private lazy var fromStack: UIStackView = {
        let labelStack = UIStackView()
        labelStack.axis = .vertical
        labelStack.alignment = .leading
        labelStack.distribution = .fillEqually
        labelStack.translatesAutoresizingMaskIntoConstraints = false
        labelStack.addArrangedSubview(self.fromLabel)
        labelStack.addArrangedSubview(self.fromCurrency)
        
        let fromStack = UIStackView()
        fromStack.translatesAutoresizingMaskIntoConstraints = false
        fromStack.axis = .horizontal
        fromStack.alignment = .fill
        fromStack.distribution = .fill
        fromStack.addArrangedSubview(labelStack)
        fromStack.addArrangedSubview(self.fromCurrencyInput)
        
        labelStack.widthAnchor.constraint(equalToConstant: 100).isActive = true
        return fromStack
    }()
    
    private lazy var toLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14, weight: .light)
        label.textColor = .white
        label.text = "TO"
        return label
    }()
    
    private lazy var toCurrency: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont.systemFont(ofSize: 24, weight: .bold)
        button.setTitleColor(.white, for: .normal)
        button.setTitle("GBP", for: .normal)
        button.addTarget(self, action: #selector(showPicker(_:)), for: .touchUpInside)
        return button
    }()
    
    private lazy var toCurrencyLabel: UITextField = {
        let textField = UITextField()
        textField.text = "0"
        textField.textColor = .white
        textField.font = UIFont.systemFont(ofSize: 40, weight: .light)
        textField.textAlignment = .right
        textField.isEnabled = false
        textField.adjustsFontSizeToFitWidth = true
        textField.minimumFontSize = 7.0
        return textField
    }()
    
    private lazy var toStack: UIStackView = {
        let labelStack = UIStackView()
        labelStack.axis = .vertical
        labelStack.alignment = UIStackView.Alignment.leading
        labelStack.distribution = .fillEqually
        labelStack.addArrangedSubview(self.toLabel)
        labelStack.addArrangedSubview(self.toCurrency)
        
        let toStack = UIStackView()
        toStack.translatesAutoresizingMaskIntoConstraints = false
        toStack.axis = .horizontal
        toStack.alignment = .fill
        toStack.distribution = .fill
        toStack.addArrangedSubview(labelStack)
        toStack.addArrangedSubview(self.toCurrencyLabel)
        
        labelStack.widthAnchor.constraint(equalToConstant: 100).isActive = true
        return toStack
    }()
    
    private lazy var convertButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Convert", for: .normal)
        button.setTitle("Converting", for: .disabled)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = #colorLiteral(red: 0, green: 0.3024611399, blue: 0.06794905622, alpha: 1)
        button.layer.cornerRadius = 20
        button.heightAnchor.constraint(equalToConstant: 40).isActive = true
        button.widthAnchor.constraint(equalToConstant: 200).isActive = true
        button.addTarget(self, action: #selector(convertCurrency), for: .touchUpInside)
        return button
    }()
    
    private var supportedEndpoints: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Currency Conversion"
        view.backgroundColor = #colorLiteral(red: 0, green: 0.5053480842, blue: 0.1135283871, alpha: 1)
        view.addSubview(fromStack)
        view.addSubview(toStack)
        view.addSubview(convertButton)
        
        NSLayoutConstraint.activate(
            [fromStack.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20),
             fromStack.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 8),
             fromStack.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8),

             toStack.topAnchor.constraint(equalTo: fromStack.bottomAnchor, constant: 20),
             toStack.leadingAnchor.constraint(equalTo: fromStack.leadingAnchor),
             toStack.trailingAnchor.constraint(equalTo: fromStack.trailingAnchor),

             convertButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
             convertButton.topAnchor.constraint(equalTo: toStack.bottomAnchor, constant: 40),]
        )
        
        listAllEndpoints(onComplete: { endpoints in
            self.supportedEndpoints = endpoints
            
            DispatchQueue.main.async {
                self.fromCurrency.setTitle(endpoints[0], for: .normal)
                self.toCurrency.setTitle(endpoints[max(endpoints.count - 1, 0)], for: .normal)
            }
        }, onError: { message in
            showMessage(message)
        })
    }
    
    @objc func showPicker(_ sender: UIView) {
        let currencyPickerVC = CurrencyPickerViewController()
        currencyPickerVC.delegate = self
        currencyPickerVC.currencies = supportedEndpoints
        currencyPickerVC.selectFromCurrency = (sender === fromCurrency)
        
        navigationController?.pushViewController(currencyPickerVC, animated: true)
    }
    
    @objc func convertCurrency() {
        guard
            let currencyAmount = Double(fromCurrencyInput.text ?? "0"),
            let fromCurrency = fromCurrency.title(for: .normal),
            let toCurrency = toCurrency.title(for: .normal)
            else { return }
        
        convertButton.isEnabled = false
        fromCurrencyInput.isEnabled = false
        self.toCurrencyLabel.text = "..."
        
        getCurrencyConversionRate(from: fromCurrency, to: toCurrency, onComplete: { rate in
            let convertedAmount = currencyAmount * rate
            
            DispatchQueue.main.async {
                self.toCurrencyLabel.text = formatCurrency(convertedAmount)
                self.convertButton.isEnabled = true
                self.fromCurrencyInput.isEnabled = true
            }
        }, onError: { message in
            showMessage(message)
            
            DispatchQueue.main.async {
                self.toCurrencyLabel.text = "Error"
                self.convertButton.isEnabled = true
                self.fromCurrencyInput.isEnabled = true
            }
        })
    }
    
}

extension CurrencyConversionViewController: CurrencyPickerViewControllerDelegate {

    func currencyPicker(_ currencyPickerViewController: CurrencyPickerViewController, didPickItemAt indexPath: IndexPath) {
        let pickFromCurrency = currencyPickerViewController.selectFromCurrency
        let newCurrency = supportedEndpoints[indexPath.row]

        if pickFromCurrency {
            fromCurrency.setTitle(newCurrency, for: .normal)
        } else {
            toCurrency.setTitle(newCurrency, for: .normal)
        }
        
        convertCurrency()
    }
    
}

extension CurrencyConversionViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let allowCharacters = CharacterSet(charactersIn: "0123456789")
        
        if string.isEmpty || string.first == "\u{8}" {
            return true
        }
        
        return string.rangeOfCharacter(from: allowCharacters) != nil
    }
    
}
