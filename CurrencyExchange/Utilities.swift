//
//  Utilities.swift
//  CurrencyExchange
//
//  Created by Cuong Nguyen on 10/17/19.
//  Copyright © 2019 Cuong Nguyen. All rights reserved.
//

import Foundation
import UIKit

fileprivate let baseURL = "https://api.exchangeratesapi.io"

func makeRequestURL(api: String, params: [String: Any] = [:]) -> URL? {
    var queryString: String = ""
    
    for param in params {
        queryString.append(contentsOf: "\(param.key)=\(param.value)&")
    }
    
    print("Request to: \(baseURL)/\(api)?\(queryString)")
    
    return URL(string: "\(baseURL)/\(api)?\(queryString)")
}

func listAllEndpoints(onComplete: @escaping (([String]) -> Void),
                      onError: ((String) -> Void)? = nil) {
    guard let url = makeRequestURL(api: "latest") else { return }
    
    URLSession.shared.dataTask(with: url) { (data, response, error) in
        guard
            let data = data,
            let json = try? JSONSerialization.jsonObject(with: data, options: []),
            let dict = json as? [String: Any],
            let supportedSymbols = dict["rates"] as? [String: Any]
        else {
            onError?("Cannot receive list of supported currencies.")
            return
        }
  
        onComplete(supportedSymbols.map { $0.key }.sorted())
    }.resume()
}

func getCurrencyConversionRate(from sourceEndpoint: String,
                               to targetEndpoint: String,
                               onComplete: @escaping (Double) -> Void,
                               onError: ((String) -> Void)? = nil) {
    guard
        let url = makeRequestURL(
            api: "latest",
            params: [
                "symbols": targetEndpoint,
                "base": sourceEndpoint,
            ]
        )
        else { return }

    URLSession.shared.dataTask(with: url) { (data, response, error) in
        guard
            let data = data,
            let json = try? JSONSerialization.jsonObject(with: data, options: []),
            let dict = json as? [String: Any],
            let result = dict["rates"] as? [String: Double]
        else {
            onError?("Cannot convert from \(sourceEndpoint) to \(targetEndpoint)")
            return
        }

        onComplete(result[targetEndpoint] ?? 0)
    }.resume()
}

func formatCurrency(_ amount: Double) -> String {
    let formatter = NumberFormatter()
    formatter.numberStyle = .decimal
    
    return formatter.string(from: NSNumber(value: amount)) ?? String(amount)
}

func showMessage(_ message: String) {
    DispatchQueue.main.async {
        guard
            let appDelegate = UIApplication.shared.delegate as? AppDelegate,
            let navViewController = appDelegate.window?.rootViewController as? UINavigationController,
            let topVC = navViewController.topViewController
            else { return }
        
        let alertVC = UIAlertController(title: "Apologise", message: message, preferredStyle: .alert)
        
        alertVC.addAction(UIAlertAction(title: "Close", style: .cancel, handler: nil))
        topVC.present(alertVC, animated: true, completion: nil)
    }
}
