//
//  CurrencyPickerViewController.swift
//  CurrencyExchange
//
//  Created by Cuong Nguyen on 10/17/19.
//  Copyright © 2019 Cuong Nguyen. All rights reserved.
//

import Foundation
import UIKit

protocol CurrencyPickerViewControllerDelegate {
    func currencyPicker(_ currencyPickerViewController: CurrencyPickerViewController, didPickItemAt indexPath: IndexPath)
}

class CurrencyPickerViewController: UITableViewController {
    
    private let reuseId = "CurrencyCell"
    public var currencies: [String] = []
    public var delegate: CurrencyPickerViewControllerDelegate? = nil
    public var selectFromCurrency: Bool = false
    
    override func viewDidLoad() {
        title = "Select Currency"
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: reuseId)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currencies.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseId, for: indexPath)
        cell.textLabel?.text = currencies[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.currencyPicker(self, didPickItemAt: indexPath)
        navigationController?.popViewController(animated: true)
    }
    
}
